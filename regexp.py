#Parser ip address of log file

import os
import re
from collections import Counter


green = '\033[32m'
default = '\033[0m'
black='\033[30m'
red='\033[31m'
orange='\033[33m'
blue='\033[34m'
purple='\033[35m'
cyan='\033[36m'
lightgrey='\033[37m'
darkgrey='\033[90m'
lightred='\033[91m'
lightgreen='\033[92m'
yellow='\033[93m'
lightblue='\033[94m'
pink='\033[95m'
lightcyan='\033[96m'


name_log_file = raw_input(yellow + "Please input name log file:" + default)
for dirpath, dirnames, filenames in  os.walk('/var/log/'):
	if name_log_file in filenames:
		with open(name_log_file,'r') as t_log:#
			n = t_log.read()
			par = re.findall('\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', n)
			count_elems = Counter(par)
			list_tuple  = count_elems.items()
			for i in  list_tuple:
				print('{}{}{}{}'.format(lightred+'ip: '+default,lightcyan+(str(i[0]))+default,lightred+' Count: '+default, lightblue+(str(i[1]))+default))
		break
else:
	print("File not found")


