This script parses the log file and gives out the types of it that are contained in it
root@test:/var/log/parsing_logs# python regexp.py
Please input name log file:test.log
ip: xx.xx.110.165 Count: 3
ip: xx.xx.249.28 Count: 83
ip: xx.xx.188.131 Count: 56
ip: xx.xx.3.17 Count: 142
ip: x.xxx.88.110 Count: 142
